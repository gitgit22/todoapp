## Poetry documentation
Go to [https://python-poetry.org/docs/](https://python-poetry.org/docs/)

1. `curl -sSL https://install.python-poetry.org | python3 -`
2. `source $HOME/.poetry/env`
3. `poetry --version`

## Start a virtual env
```
poetry shell
```

## Install all required dependency 
```
poetry install
```

## Getting started

```
python main.py
```

import gi
from todo.TodoItem import TodoItem 
import todo.dbconnect as db

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class MyWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title = "Todo App", vexpand = True)
        self.set_size_request(600, 500)
        
        self.outer_box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL, spacing = 6)
        
        self.add(self.outer_box)

        self.scrolledwindow = Gtk.ScrolledWindow()
        # scrolledwindow.set_hexpand(True)
        self.scrolledwindow.set_vexpand(True)
        
        self.listbox = Gtk.ListBox()
        self.listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.list_previous_tasks()

        self.scrolledwindow.add(self.listbox)
        self.outer_box.pack_start(self.scrolledwindow, True, True, 0)

        self.todoInputBox = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL, spacing = 5)

        self.todoInputEntry = Gtk.Entry(editable = True)
        self.todoInputEntry.set_placeholder_text("Add a task...")
        self.todoInputBox.pack_start(self.todoInputEntry, True, True, 5)
        self.todoInputEntry.connect("activate", self.on_add_clicked)

        self.addTodoButton = Gtk.Button(label = "Add task")
        self.addTodoButton.connect("clicked", self.on_add_clicked)
        self.todoInputBox.pack_start(self.addTodoButton, False, True, 5)
        
        self.outer_box.pack_start(self.todoInputBox, False, False, 5)

        self.todoInputEntry.grab_focus_without_selecting()
    

    def list_previous_tasks(self):
        tasks = db.get_all_tasks()
        for task in tasks:
            todoitem = TodoItem(task[0], task[1])
            self.listbox.add(todoitem)
        self.show_all()


    def on_add_clicked(self, button):
        if self.todoInputEntry.get_text() != "":
            todotext = self.todoInputEntry.get_text()
            todoitem = TodoItem(todotext, "not done")

            db.conn.execute("""
            INSERT INTO tasks (details, status) 
            VALUES (?, ?);""", (todotext, 'not done'))
            db.conn.commit()

            self.listbox.add(todoitem)
            self.todoInputEntry.set_text("")
            self.todoInputEntry.grab_focus_without_selecting()
        self.show_all()

def quit_app(app):
    db.conn.close()
    Gtk.main_quit()

win = MyWindow()
win.connect("destroy", quit_app)
win.show_all()
Gtk.main()


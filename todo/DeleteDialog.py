import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class DeleteDialog(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title = "Confirm delete", flags = Gtk.DialogFlags.MODAL)
        self.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)
        self.set_default_size(300, 50)

        label = Gtk.Label(label = "Confirm deleting this task from the todo list?")
        box = self.get_content_area()
        box.add(label)
        self.show_all()


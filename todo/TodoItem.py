import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gdk

import todo.dbconnect as db
from todo.DeleteDialog import DeleteDialog

class TodoItem(Gtk.ListBoxRow):
    """Model class for to-do items"""
    
    def __init__(self, todotext, status):
        super().__init__()
        
        self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.add(self.hbox)

        self.check_done = Gtk.CheckButton()
        self.check_done.connect("toggled", self.on_check_done_toggled)
        self.status = status
        if self.status == "done":
            self.check_done.set_active(True)
        self.hbox.pack_start(self.check_done, False, False, 5)

        self.todoentry = Gtk.Entry(editable = False)
        self.todoentry.set_text(todotext)
        self.task_before_edit = str()
        self.hbox.pack_start(self.todoentry, True, True, 0)

        self.edit_button = Gtk.Button(label = "Edit")
        # self.edit_button.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 0.75))
        self.edit_button.connect("clicked", self.on_edit_clicked)
        self.hbox.pack_start(self.edit_button, False, True, 0)
        
        self.delete_button = Gtk.Button(label = "Delete")
        self.delete_button.connect("clicked", self.on_delete_clicked)
        self.hbox.pack_start(self.delete_button, False, True, 0)
    
    def on_check_done_toggled(self, widget):
        if self.status == "done":
            return
        if widget.get_active():
            db.conn.execute("""
            UPDATE tasks
            SET status = 'done'
            WHERE details = ?""", (self.todoentry.get_text(), ))
            db.conn.commit()
    
    def on_edit_clicked(self, button):        
        if button.get_label() == "Edit":
            self.task_before_edit = self.todoentry.get_text()
            self.todoentry.set_editable(True)
            self.todoentry.grab_focus_without_selecting()
            button.set_label("Update")
        elif button.get_label() == "Update":
            db.conn.execute("""
            UPDATE tasks
            SET details = ?
            WHERE details = ?""", (self.todoentry.get_text(), self.task_before_edit))
            db.conn.commit()

            self.todoentry.set_editable(False)
            button.set_label("Edit")
        
    def on_delete_clicked(self, button):
        deleteDialog = DeleteDialog(self)
        response = deleteDialog.run()
        
        if response == Gtk.ResponseType.OK:
            db.conn.execute("""
            DELETE FROM tasks
            WHERE details = ?;""", (self.todoentry.get_text(), ))
            db.conn.commit()

            self.destroy()
        
        deleteDialog.hide()

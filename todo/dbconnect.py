import os
import sqlite3

dbFileName = "todos.db"
schemaFileName = "todoSchema.sql"

conn = sqlite3.connect(dbFileName)

if not os.path.exists(dbFileName):
    with open("todoSchema.sql", 'rt') as f:
        schema = f.read()
        conn.executescript(schema)
    print("DB didn't exist, created DB and schema")

def get_all_tasks():
    cursor = conn.cursor()
    cursor.execute("""
        SELECT details, status
        FROM tasks;
        """)
    return cursor.fetchall()

# if __name__ != "__main__":
#     connect()

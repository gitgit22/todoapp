CREATE TABLE tasks (
    id          integer primary key autoincrement not null,
    details     text,
    status      text
);